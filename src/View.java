


public class View {

    // Constant messages

    public static final String INPUT_REQUIREMENT_HELLO = "Input «Hello» (without quotes)";
    public static final String INPUT_REQUIREMENT_WORLD = "Input «world!» (without quotes)";
    public static final String WRONG_INPUT_WORD = "Wrong input! Try again please!";

    //Console output methods
    public void printMessage (String message){
        System.out.println(message);
    }

    public void printHelloWorld (String hello, String world){
        System.out.println(hello + " " + world);
    }

}
