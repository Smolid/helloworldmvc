


public class Model {

    //Constants

    private static final String HELLO = "Hello";
    private static final String WORLD = "world!";

    //Initialized accepting Strings

    private String firstWord = "o";
    private String secondWord = "o";


    //Setters

    public void setFirstWord(String firstWord) {
        this.firstWord = firstWord;
    }

    public void setSecondWord(String secondWord) {
        this.secondWord = secondWord;
    }

    // Getters with "database-imitation" result check

    public String getFirstWord() {
        if(helloCheck(firstWord)) {
            return firstWord;
        } else {
            return "o";
        }
    }


    public String getSecondWord(){
        if(worldCheck(secondWord)){
            return secondWord;
        } else {
            return "o";
        }

    }

    // "Database-imitation" check (тут могла быть проверка на "свободность" имени пользователя если бы мы вводили имя пользователя)
    private boolean helloCheck(String firstWord){
        if(firstWord.equals(HELLO)){
            return true;
        }else{
            return false;
        }
    }

    private boolean worldCheck(String secondWord){
        if(secondWord.equals(WORLD)){
            return true;
        }else{
            return false;
        }
    }
}
