

import java.util.Scanner;

public class Controller {

    // Constants


    //Constructor
    private Model model;
    private View view;

    public Controller(Model model, View view){
        this.model=model;
        this.view=view;
    }

    public void processUser(){
        Scanner input = new Scanner(System.in);

        // Input methods with "model-check" result check

        while (model.getFirstWord().equals("o")){
            model.setFirstWord(inputStringFirstValueWithScanner(input));
            if(model.getFirstWord().equals("o"))
                wrongWord();
        }

        while (model.getSecondWord().equals("o")) {
            model.setSecondWord(inputStringSecondValueWithScanner(input));
            if (model.getSecondWord().equals("o"))
                wrongWord();
        }

        //Result output method
        view.printHelloWorld(model.getFirstWord(),model.getSecondWord());

    }
    private void wrongWord(){
        view.printMessage(View.WRONG_INPUT_WORD);
    }

    // Input methods

    private String inputStringFirstValueWithScanner(Scanner input){
        view.printMessage(View.INPUT_REQUIREMENT_HELLO);
        while (!input.hasNextLine()){

            input.next();
        }
        return input.next();
    }

    private String inputStringSecondValueWithScanner(Scanner input){
        view.printMessage(View.INPUT_REQUIREMENT_WORLD);
        while (!input.hasNextLine()){

            input.next();
        }
        return input.next();
    }
}
