public class Main {
    /*
    Написать программу, которая получает из командной строки сначала слово “Hello”, потом слово “world!”.
    Другие вводы игнорировать с соответствующим комментарием в командной строке.
    Из этих слов собирается предложение и выводится на экран.
     */
    public static void main(String[] args) {
        Model model = new Model();
        View view = new View();
        Controller controller = new Controller(model, view);

        controller.processUser();
    }
}
